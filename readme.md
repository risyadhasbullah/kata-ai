First time setup:

1. Make sure that you have these applications installed:
Python 2.7
https://www.python.org/downloads/

pip (included with Python 2 >=2.7.9 or Python 3 >=3.4)
https://pip.pypa.io/en/stable/installing/

postgres
http://postgresguide.com/setup/install.html

virtualenv
https://virtualenv.pypa.io/en/stable/installation/

2. Setup database. Please do only until the “Create a Database and Database User” section. Do not do the “Install Django within a virtual environment”
https://www.digitalocean.com/community/tutorials/how-to-use-postgresql-with-your-django-application-on-ubuntu-14-04

3. Still on the postgres login, run this command ALTER USER <username> CREATEDB;
 This is required to run the django tests.

4. After cloning, make a virtualenv outside the project folder.
   ex: virtualenv example_env

5. Activate the env, and install all the dependencies from requirements.txt
   ex:
   $ source example_env/bin/activate
	 $ cd kata
	 $ pip install -r requirements.txt

6. Duplicate .env.example file in the inner kata folder and name it to .env

7. Add your database credential in your .env file

8. Migrate, and test your django (runserver).
   ex:
   $ python manage.py makemigrations
   $ python manage.py migrate
