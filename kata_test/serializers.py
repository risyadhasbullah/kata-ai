from .models import Data
from rest_framework import serializers

# Serialize data to json with url and data fields.
class DataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Data
        fields = ('url', 'data')
