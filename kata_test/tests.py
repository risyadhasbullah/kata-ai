from django.test import TestCase
from .models import Data
from rest_framework.test import APIClient
from django.core.urlresolvers import reverse
from rest_framework import status

def create_dummy_data():
    data = Data.objects.create(data='Test')
    return data

def generate_data():
    data = {'data': 'Hello'}
    return data


class DataModelTestCase(TestCase):

    '''Set up the information needed to create a data'''
    def setUp(self):
        self.data = create_dummy_data()
        print(self.data)

class DataViewTestCase(TestCase):

    '''Define the test client and test variables for api'''
    def setUp(self):
        self.client = APIClient()
        self.data= generate_data()
        self.response = self.client.post(
            reverse('data-list'),
            self.data,
            format="json")

    def test_api_can_post_data(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_api_can_get_a_data(self):
        data = Data.objects.get()
        response = self.client.get(
            reverse('data-detail', kwargs={'pk': data.id}),
            format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, data.data)

    def test_api_can_put_a_data(self):
        data = Data.objects.get()
        self.data['Data'] = 'World'
        res = self.client.put(
            reverse('data-detail', kwargs={'pk': data.id}),
            self.data, format='json'
        )
        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_api_can_delete_data(self):
        data = Data.objects.get()
        response = self.client.delete(
            reverse('data-detail', kwargs={'pk': data.id}),
            format='json',
            follow=True)
        self.assertEquals(response.status_code, status.HTTP_204_NO_CONTENT)
