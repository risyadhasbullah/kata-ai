from django.shortcuts import render
from .serializers import DataSerializer
from .models import Data
from rest_framework import viewsets

#CRUD Data
class DataViewSet(viewsets.ModelViewSet):
    queryset = Data.objects.all()
    serializer_class = DataSerializer
