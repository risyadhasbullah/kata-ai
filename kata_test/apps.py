from django.apps import AppConfig


class KataTestConfig(AppConfig):
    name = 'kata_test'
