from django.db import models


class Data(models.Model):
    data = models.CharField(max_length=200)

    def __str__(self):
        return '%s' % (self.data)
